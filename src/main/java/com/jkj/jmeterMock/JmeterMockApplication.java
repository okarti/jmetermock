package com.jkj.jmeterMock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JmeterMockApplication {

	public static void main(String[] args) {
		SpringApplication.run(JmeterMockApplication.class, args);
	}

}
