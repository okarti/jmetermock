// TODO
// - Добавить функцию сохранения входящих запросов (для актуализации в будущем)


package com.jkj.jmeterMock;

import org.apache.jmeter.save.SaveService;
import org.apache.jmeter.util.JMeterUtils;


import org.apache.logging.log4j.core.lookup.StrSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@RestController
public class JMeterMockController {


    private String jmeterHome;
    private Boolean debug = true;

    File in = new File(String.valueOf(FileSystems.getDefault().getPath("classes","research.jmx")));
    Logger logger = LoggerFactory.getLogger(JMeterMockController.class);
    private static Map<String, String> templates = new HashMap<String, String>();


    @Value("${delay}")
    private Integer delay;

    @Value("${templateNameNode}")
    private String templateNameNode;

    @Value("#{'${varNames}'.split(',')}")
    private List<String> varNames;

    @Value("#{'${attribNames}'.split(',')}")
    private List<String> attribNames;

    @Value("${markerNodeName}")
    private String markerNodeName;

    @Value("${markerNodeValue}")
    private String markerNodeValue;

    @Value("${dateTimeFormat}")
    private String dateTimeFormat;

    @Value("${autoreload_template}")
    private Boolean autoreload_template;

    public JMeterMockController(@Value("${jmeterHome}") final String jmeterHome) throws IOException {
        logger.info(jmeterHome);

        if (!debug) {
            // Initialize Properties, logging, locale, etc.
            // FIXME какие то проблемы с путями. Похоже похоже что для тестов спринга директория не "target". Перенести в application.property
            String propFile = new File("classes" + File.separator + "jmeter.properties").getAbsolutePath();
            logger.info(propFile);
            JMeterUtils.loadJMeterProperties(propFile);
            JMeterUtils.setJMeterHome(jmeterHome);
            JMeterUtils.initLogging();// you can comment this line out to see extra log messages of i.e. DEBUG level
            JMeterUtils.initLocale();

            // Initialize JMeter SaveService
            SaveService.loadProperties();
        }
    }

    public static Document loadXMLFromString(String xml) throws Exception
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xml));
        return builder.parse(is);
    }

    @PostMapping("/")
    public String greeting(@RequestBody String body) throws Exception {

        logger.trace(templateNameNode);

        logger.info("Start request handling");
        logger.debug(body);

        logger.trace("Start parse");
        Document dom = loadXMLFromString(body);

        // Начитаем переменные значений узлов
        Map<String, String> values = new HashMap<String, String>();
        for (String varName : varNames) {
            NodeList nodeList = dom.getElementsByTagNameNS("*", varName);
            if (nodeList.getLength() > 0) {
                String varValue = dom.getElementsByTagNameNS("*", varName).item(0).getTextContent();
                values.put(varName, varValue);
            }
        }

        // Начитаем переменные атрибутов узлов
        XPathFactory xpathFactory = XPathFactory.newInstance();
        String xpathExpression = "";
        for (String attribName : attribNames) {
            xpathExpression = "//*/@" + attribName;

            XPath xpath = xpathFactory.newXPath();
            XPathExpression expr = xpath.compile(xpathExpression);
            NodeList nodes = (NodeList) expr.evaluate(dom, XPathConstants.NODESET);

            if (nodes.getLength() > 0) {
                String attribValue = nodes.item(0).getNodeValue();
                values.put("attrib_" + attribName, attribValue);
            } else {
                logger.trace("attrib not found");
            }
        }

        // Прочитаем переменную Маркер
        String requestMarkerValue = "NOT_FOUND";
        NodeList nList = dom.getElementsByTagNameNS("*", markerNodeName);
        if (nList.getLength() > 0){
            requestMarkerValue = nList.item(0).getTextContent();
        }

        // Определимся какой шаблон использовать
        nList = dom.getElementsByTagNameNS("*", templateNameNode);
        List<String> templateKeyValues = new ArrayList<String>();;
        for (int i = 0; i < nList.getLength(); i++) {
            Element ele = (Element) nList.item(i);
            templateKeyValues.add(ele.getTextContent());
        }
        logger.trace(requestMarkerValue);
        logger.trace(markerNodeValue);
        if (requestMarkerValue.equals(markerNodeValue)) {
            templateKeyValues.add(markerNodeName);
            templateKeyValues.add(requestMarkerValue);
        }

        String templateName = String.join("_", templateKeyValues);
        logger.debug(templateName);

        String content;
        if ((!templates.containsKey(templateName)) || autoreload_template) {
            Path path = Paths.get("classes","templates", templateName);
            logger.debug(path.toString());
            content = new String(Files.readAllBytes(path), Charset.forName("UTF-8"));
            templates.put(templateName, content);
        }
        content = templates.get(templateName);

        // Добавить время
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateTimeFormat);
        String formatDateTime = now.format(formatter);
        values.put("__timestamp()", formatDateTime);
        values.put("__time()", formatDateTime);

        StrSubstitutor sub = new StrSubstitutor(values, "${", "}");
        String response = sub.replace(content);

        logger.debug(response);
        Thread.sleep(delay);
        logger.info("End request handling");
        return response;
    }
}

